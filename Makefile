SYS	= $(shell uname -s)
MAKE = make
CC = gcc
LD = ld
ASM64 = yasm -f elf64 -DYASM -D__x86_64__ -DPIC -g dwarf2 
CFLAGS = -g -Wall -fno-stack-protector -nostdlib -pedantic
LDFLAGS = -shared
LDTESTFLAGS	= -m elf_x86_64 --dynamic-linker /lib64/ld-linux-x86-64.so.2

LIBS = -lmini

OBJ = libmini64.o libmini.o

.PHONY: all clean test alarm1 alarm2 alarm3 jmp1

all: libmini.so

test: all alarm1 alarm2 alarm3 jmp1

%.o: %.asm
	$(ASM64) $^ -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) -fPIC $^ -o $@

libmini.so: $(OBJ)
	$(LD) $(LDFLAGS) $^ -o $@


alarm1: start.o
	$(CC) -c $(CFLAGS) -I. -I.. -DUSEMINI alarm1.c -o alarm1.o
	$(LD) $(LDTESTFLAGS) -o $@ alarm1.o start.o  -L. -L.. $(LIBS)
	LD_LIBRARY_PATH=. ./$@

alarm2: start.o
	$(CC) -c $(CFLAGS) -I. -I.. -DUSEMINI alarm2.c -o alarm2.o
	$(LD) $(LDTESTFLAGS) -o $@ alarm2.o start.o  -L. -L.. $(LIBS)
	LD_LIBRARY_PATH=. ./$@

alarm3: start.o
	$(CC) -c $(CFLAGS) -I. -I.. -DUSEMINI alarm3.c -o alarm3.o
	$(LD) $(LDTESTFLAGS)  -o $@ alarm3.o start.o -L. -L.. $(LIBS)
	LD_LIBRARY_PATH=. ./$@

jmp1: start.o
	$(CC) -c $(CFLAGS) -I. -I.. -DUSEMINI jmp1.c -o jmp1.o
	$(LD) $(LDTESTFLAGS) -o $@ jmp1.o start.o -L. -L.. -lmini
	LD_LIBRARY_PATH=. ./$@

clean:
	rm -rf *.o *.so alarm1 alarm2 alarm3 jmp1
