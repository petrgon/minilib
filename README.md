**Title:** Homework #3

**Course:** Advanced Programming in the UNIX Environment

**University:** NCTU

**Author:** Bc. Petr Gondek

**Implemented Goals:**
- [x] alarm
- [x] sigprocmask
- [x] sigpending
- [x] functions to handle sigset_t data type
- [x] setjmp and longjmp
- [x] signal and sigaction

