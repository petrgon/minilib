
%macro gensys 2
	global sys_%2:function
sys_%2:
	push	r10
	mov	r10, rcx
	mov	rax, %1
	syscall
	pop	r10
	ret
%endmacro

; RDI, RSI, RDX, RCX, R8, R9

extern	errno
extern  sigprocmask

	section .data

	section .text

	gensys   0, read
	gensys   1, write
	gensys   2, open
	gensys   3, close
	gensys   9, mmap
	gensys  10, mprotect
	gensys  11, munmap
	gensys  13, rt_sigaction
	gensys  14, rt_sigprocmask
	gensys  15, rt_sigreturn
	gensys  22, pipe
	gensys  32, dup
	gensys  33, dup2
	gensys  34, pause
	gensys  35, nanosleep
	gensys  37, alarm
	gensys  57, fork
	gensys  60, exit
	gensys  79, getcwd
	gensys  80, chdir
	gensys  82, rename
	gensys  83, mkdir
	gensys  84, rmdir
	gensys  85, creat
	gensys  86, link
	gensys  88, unlink
	gensys  89, readlink
	gensys  90, chmod
	gensys  92, chown
	gensys  95, umask
	gensys  96, gettimeofday
	gensys 102, getuid
	gensys 104, getgid
	gensys 105, setuid
	gensys 106, setgid
	gensys 107, geteuid
	gensys 108, getegid
	gensys 127, rt_sigpending

	global open:function
open:
	call	sys_open
	cmp	rax, 0
	jge	open_success	; no error :)
open_error:
	neg	rax
%ifdef NASM
	mov	rdi, [rel errno wrt ..gotpc]
%else
	mov	rdi, [rel errno wrt ..gotpcrel]
%endif
	mov	[rdi], rax	; errno = -rax
	mov	rax, -1
	jmp	open_quit
open_success:
%ifdef NASM
	mov	rdi, [rel errno wrt ..gotpc]
%else
	mov	rdi, [rel errno wrt ..gotpcrel]
%endif
	mov	QWORD [rdi], 0	; errno = 0
open_quit:
	ret

	global sleep:function
sleep:
	sub	rsp, 32		; allocate timespec * 2
	mov	[rsp], rdi		; req.tv_sec
	mov	QWORD [rsp+8], 0	; req.tv_nsec
	mov	rdi, rsp	; rdi = req @ rsp
	lea	rsi, [rsp+16]	; rsi = rem @ rsp+16
	call	sys_nanosleep
	cmp	rax, 0
	jge	sleep_quit	; no error :)
sleep_error:
	neg	rax
	cmp	rax, 4		; rax == EINTR?
	jne	sleep_failed
sleep_interrupted:
	lea	rsi, [rsp+16]
	mov	rax, [rsi]	; return rem.tv_sec
	jmp	sleep_quit
sleep_failed:
	mov	rax, 0		; return 0 on error
sleep_quit:
	add	rsp, 32
	ret

;  ==================== mine ====================
	global alarm:function
alarm:
    call sys_alarm
	ret

	global restorer:function
restorer:
    mov rax, 0x0f
    syscall

	global setjmp:function
setjmp:
	;64 bit registers dont have an "e" they have an "r" 
	;rbp, the frame pointer, contains a pointer to the start of the current stack frame
	;rip, the instruction pointer, contains a pointer to the next instruction to execute
	;rsp, top of stack (the first element out)
	;rsi, source index, second param
	;rdi, destination index, first param
	;rax is used for function return values

	; QWORD [rdi] take 8 bites from
	; DWORD takes 4 bites

    mov QWORD [rdi], rbx    			; jmp_buf[0] = rbx
 	pop rcx								; get previous value of rsp, before call\n"
    mov QWORD [rdi+8], rsp  			; jmp_buf[1] = rsp before call
	push rcx
    mov QWORD [rdi+16], rbp 			; jmp_buf[2] = rbp
    mov QWORD [rdi+24], r12 			; jmp_buf[3] = r12
    mov QWORD [rdi+32], r13 			; jmp_buf[4] = r13
    mov QWORD [rdi+40], r14 			; jmp_buf[5] = r14
    mov QWORD [rdi+48], r15 			; jmp_buf[6] = r15
    mov QWORD [rdi+56], rcx				; jmp_buf[7] = rip

	lea rdx, [rdi+64]					; rdx = &(jmp_buf.mask) 
	mov rdi, 0 							; SIG_BLOCK
	xor rsi, rsi 						; rsi = null
	;mov rdx, QWORD rcx
	mov rcx, 4
	call sys_rt_sigprocmask
    
	xor rax, rax 		    			; rax = 0
    ret
	
    global longjmp:function
longjmp:
	mov rax, rsi 				; second param as ret val
    mov rbx, QWORD [rdi]
    mov rsp, QWORD [rdi+8]
    mov rbp, QWORD [rdi+16]
    mov r12, QWORD [rdi+24]
    mov r13, QWORD [rdi+32]
    mov r14, QWORD [rdi+40]
    mov r15, QWORD [rdi+48]	

	lea rcx, [rdi+56]
	push rcx
	lea rsi, [rdi+64]					; rsi = &(jmp_buf.mask) 
	mov rdi, 2 							; SIG_SETMASK
	xor rdx, rdx 						; rdx = null
	;mov rdx, QWORD rcx
	mov rcx, 4
	call sys_rt_sigprocmask
	pop rcx

	jmp [rcx]  
	ret
